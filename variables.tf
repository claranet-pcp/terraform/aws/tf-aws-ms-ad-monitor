## General vars
variable "customer" {
  description = "Name of the customer"
  type        = "string"
}

variable "envname" {
  description = "This label will be added after 'name' on all resources, and be added as the value for the 'Environment' tag where supported"
  type        = "string"
}

variable "new_users" {
  description = "users to be added to AD"
  type        = "string"
}

variable "domain" {
  type = "string"
}

variable "ad_server" {
  type = "string"
}

variable "notification_days" {
  type    = "string"
  default = 3
}

variable "cron_schedule" {
  type    = "string"
  default = "cron(0 */10 * * * ? *)"
}

variable "remove_computer_accounts_after" {
  type        = "string"
  default     = "8"
  description = "weeks until a dormant computer account is removed"
}
