tf-aws-ms-ad-monitor
========================

This module is a re-write of the Lambda original. This version uses AWS SSM to deliver a script to a specific instance. THat instance then runs the powershell against the given domain.

The module does the following :-

1) for a given set of users in var.monitored_ad_users, the user get created in AD and added to the given AD group name
2) new users are notified by email of the newly created account, including domain and password
3) all ad users are added to the password expiry notification, assuming they have a valid email address
4) removes any AD computer account that is older than 8 weeks

All resources are prefixed with customer_name-envname.

The monitored_ad_users variable takes the following format

logical_name - used to name the SNS topic. This must be compliant with SNS resource naming restrictions
userid - userid of the account that you wish to be monitored
email - email address to receive alert if the above account approaches it password expiry  

Multiple accounts can be monitored by using the following formay

monitored_ad_users    = ["logical_name1,userid1,valid_email_address1","logical_name2,userid2,valid_email_address2"........"logical_name_x,userid_x,valid_email_address_x"]

Users removed from the monitored_ad_users variable are not deleted from AD. This has to be done manually.

The process runs at 10 minutes, however that schedule can be changed by passing an alternative value for cron_schedule. Currently the variable defaults to "cron(0 */10 * * * ? *)"


Prerequisites
-------------

There are some specific requirements when using this module, these are listed below;

- For successful notification , all monitored users must be using the default domain policy
- The instance being targetted by SSM to run the script must have access to AD & SNS
- The instance being targetted by SSM to run the script must have dstools enabled


Usage
-----

Declare a module in your Terraform file, for example:


```js
module "ms_ad_tools" {
  source                          = "../../tf-aws-ms-ad-tools"
  customer                        = "${var.customer}"
  envname                         = "${var.envname}"
  new_users                       = "james.hollins,james.hollins@claranet.uk,domain admins|ian.dellbridge,ian.dellbridge@claranet.uk,domain admins"
  domain                          = "${var.ads_fq_domain_name}"
  ad_server                       = "${element(module.ads.ads_dns, 0)}"
  notification_days = "5"
  remove_computer_accounts_after  = "${var.remove_computer_accounts_after}"
}
```

Variables
---------

- `customer`                      - name of customer
- `envname`                       - name of environment
- `new_users`                     - a list of monitored ad users. see above for format
- `domain`                        - domain for the actove directory being monitored
- `ad_server`                     - ip address of a single AD domain controller
- `notification_days`             - how many days before password expiry that the alert start sending.
- `cron_schedule`     	          - crontab to trigger the Lambda - default set to daily 1am
- `remove_computer_accounts_after`- weeks until a dormant computer account is removed from the domain   	      



















