data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

resource "aws_ssm_document" "ad_monitor" {
  name          = "${var.customer}-${var.envname}-ad_monitor"
  document_type = "Command"
  content       = "${data.template_file.ad_monitor_run.rendered}"
}

data "template_file" "ad_monitor_run" {
  template = "${file("${path.module}/include/ad_monitor.tmpl")}"

  vars {
    new_users_all                  = "${var.new_users}"
    domain                         = "${var.domain}"
    ad_server                      = "${var.ad_server}"
    aws_account_id                 = "${data.aws_caller_identity.current.account_id}"
    aws_region                     = "${data.aws_region.current.name}"
    customer                       = "${var.customer}"
    envname                        = "${var.envname}"
    notification_days              = "${var.notification_days}"
    remove_computer_accounts_after = "${var.remove_computer_accounts_after}"
  }
}

resource "aws_ssm_maintenance_window" "ad_monitor" {
  name     = "${var.customer}-${var.envname}-ad_monitor"
  schedule = "${var.cron_schedule}"
  duration = 2
  cutoff   = 1
}

resource "aws_ssm_maintenance_window_target" "ad_monitor" {
  window_id     = "${aws_ssm_maintenance_window.ad_monitor.id}"
  resource_type = "INSTANCE"

  targets {
    key    = "tag:ad_tools"
    values = ["yes"]
  }
}

resource "aws_ssm_maintenance_window_task" "ad_monitor" {
  window_id        = "${aws_ssm_maintenance_window.ad_monitor.id}"
  task_type        = "RUN_COMMAND"
  task_arn         = "ad_monitor"
  priority         = 1
  service_role_arn = "${aws_iam_role.ad_monitor_ssm_maintenance_window.arn}"
  max_concurrency  = "100"
  max_errors       = "10"

  targets {
    key    = "WindowTargetIds"
    values = ["${aws_ssm_maintenance_window_target.ad_monitor.*.id}"]
  }
}

resource "aws_iam_role" "ad_monitor_ssm_maintenance_window" {
  name = "${var.customer}-${var.envname}-ad_monitor-ssm-mw-role"
  path = "/system/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com","ssm.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "role_attach_ad_monitor_mw" {
  role       = "${aws_iam_role.ad_monitor_ssm_maintenance_window.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonSSMMaintenanceWindowRole"
}
